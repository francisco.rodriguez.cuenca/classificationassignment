## Load libraries --------------------------------------------------------------------------------
library(caret)
library(ggplot2)
library(ROCR) #for plotting ROC curves.
library(MLTools)
library(GGally)
library(tidyverse)
library(gridExtra) #permite juntar graficos en una sola imagen
library(rpart)
library(rpart.plot)
library(partykit)

## Load file -------------------------------------------------------------------------------------

fdata <- read.table("TrainingSet1001_MIC.dat", sep = " ", header = TRUE, stringsAsFactors = FALSE)

#Convert output variable to factor

fdata$Y <- factor(fdata$Y)
str(fdata)

ggpairs(
  fdata,
  aes(color = Y, alpha = 0.3),
  upper = list(continuous = wrap("cor", family="sans", size = 2))
) +
  theme(
    axis.text=element_text(size=4),
    text = element_text(size=5),
    axis.text.y.right = element_text(angle=90, hjust=1)
  )

set.seed(140)
set.seed(588612)
set.seed(1984)
set.seed(600030)
trainIndex <- createDataPartition(fdata$Y, p = 0.8, list = FALSE, times = 1)  
fTR <- fdata[trainIndex,]

fTS <- fdata[-trainIndex,]
fTR_eval <- fTR
fTS_eval <- fTS
ctrl <- trainControl(method = "cv", number = 10, summaryFunction = defaultSummary, classProbs = TRUE)

set.seed(100)
knn.fit = train(form = Y ~ X10 + X9 + X6,data = fTR,method = "knn",preProcess = c("center","scale"),tuneGrid = data.frame(k = seq(2,20,1)),trControl = ctrl, metric = "Accuracy")
knn.fit 
knn.fit$finalModel

fTR_eval$knn_prob <- predict(knn.fit, type="prob" , newdata = fTR) 
fTR_eval$knn_pred <- predict(knn.fit, type="raw" , newdata = fTR)

confusionMatrix(data = fTR_eval$knn_pred, reference = fTR_eval$Y, positive = "YES") 

#test
fTS_eval$knn_prob <- predict(knn.fit, type="prob" , newdata = fTS) 
fTS_eval$knn_pred <- predict(knn.fit, type="raw" , newdata = fTS) 

confusionMatrix(fTS_eval$knn_pred, fTS_eval$Y, positive = "YES")
PlotClassPerformance(fTR_eval$Y, fTR_eval$knn_prob, selClass = "YES")
