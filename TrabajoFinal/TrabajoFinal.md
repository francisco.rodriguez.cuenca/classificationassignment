# Memoría de la practica de Machine Learning: Clasificación
*Santiago Cebellán Martínez, Aitor Perez Ezkurra, Francisco Rodriguez Cuenca*

## Análisis Exploratorio

Representamos en primer lugar todos los registros por variable contenidos en el dataset Diabetes.csv.

<center>
  <figure>
    <img src="AnalisisAntes.png" alt=".." title="Fig 1" />
    <figcaption>Fig 1. Analisis inicial</figcaption>
  </figure>
</center>

Nos damos cuenta de dos problemas:

1. Las clases Diabetico y NoDiabetico están descompensadas, es decir, tenemos aproximadamente el doble de registros no diabeticos que diabeticos.

2. Existen cuatro variables con valores "0". Por definición, ninguna de estas variables pueden referirse a valores reales. Dichas variables son Insulin (insulina), SkinThickness(grosor de la piel), BodyMassIndex (índice de masa corporal) y BloodPress (presión sanguínea)

Optamos por retirar del dataset completo los registros que tengan estos valores atípicos.

Sin embargo, las clases no las podemos equilibrar en el dataset de partida, ya que afectaría al subconjunto de test.

Por ello, optamos a dividir los registros en 80% training, 20% test y equilibrar las clases en el subconjunto de training duplicando los registros de diabéticos.

Este sería el análisis de variables en training:

<center>
  <figure>
    <img src="AnalisisTraining.png" alt=".." title="Fig 2" />
    <figcaption>Fig 2. Clases compensadas sobre training</figcaption>
  </figure>
</center>

Utilizando ggpairs, nos damos cuenta que BloodPressure y SkinThickness son variables poco significativas. Además existe una nivel de correlación entre las variables Age y Pregnant del 0.68, es de esperar ya que una mayor edad permite tener un número mayor de embarazos.


## Identificación de modelos

### Regresión logística

Un modelo de regresión logística es un modelo de regresión nos permite obtener una variable respuesta categórica a partir de variables explicativas continuas.

Primero entrenamos el modelo a partir de todas las variables explicativas para hacernos una idea sobre el modelo. Además nos permitirá hacernos una idea de cuales variables son importantes.

Este es el análisis de coeficientes sobre cada variable. 

Variable | Estimate | Std. Error | z value | Pr(>z)
---|---|---|---|---
PREGNANT | 0.29906 | 0.18185 | 1.644 | 0.100073
GLUCOSE | 1.04343 | 0.17185 | 6.072  | 1.27e-09 ***
BLOODPRESS | -0.04257 | 0.13584  | -0.313  | 0.753976
SKINTHICKNESS  | 0.33204 | 0.16783 | 1.978  | 0.047881 *
INSULIN | 0.08339 | 0.15646 | 0.533  | 0.594049
BODYMASSINDEX | 0.44612 | 0.17616 | 2.532  | 0.011327 *
PEDIGREEFUNC | 0.51133 | 0.14216 | 3.597  | 0.000322 ***
AGE | 0.31274 | 0.19440 | 1.609  | 0.107677

Observamos que las únicas variables con efecto estadísticamente significativos son Glucose, PedigreeFunc, BodyMassIndex y SkinThickness, de manera que para la siguiente iteración eliminamos el resto de variables.

Además, en nuestras pruebas comprobamos que debido a que Age y Pregnant están correlacionadas, si mantenemos pregnant y elminamos Age, pregnant pasa a ser estadísticamente significativa.

Volvemos a entrenar el modelo con las variables PREGNANT, GLUCOSE, SKINTHICKNESS, BODYMASSINDEX y PEDIGREEFUNC. Aprovechando que ya no usamos la variable INSULINE vamos a recuperar todos los datos que perdimos debido a los valores nulos de dicha variables.

Variable | Estimate | Std. Error | z value | Pr(>z)
---|---|---|---|---
PREGNANT | 0.71005 | 0.11474 | 6.188 | 6.08e-10 ***
GLUCOSE | 1.18874 | 0.12865 | 9.240  | < 2e-16 ***
SKINTHICKNESS  | 0.02105 | 0.14273 | 0.148  | 0.883
BODYMASSINDEX | 0.58293 | 0.14937 | 3.903  | 9.52e-05 ***
PEDIGREEFUNC | 0.54723 | 0.12802 | 4.275  | 1.92e-05 ***

Ahora ya casi todas las variables son significativas, solo falla SKINTHICKNESS que en el anterior modelo estaba a punto de no ser significativo para un nivel de significación del 95%. La eliminamos y asi ganamos accesos a todas aquellas filas que tenian valores imposibles para SKINTHICKNESS.

Variable | Estimate | Std. Error | z value | Pr(>z)
---|---|---|---|---
PREGNANT | 0.51811 | 0.09287 | 5.579 | 2.42e-08 ***
GLUCOSE | 1.27828 | 0.10875 | 9.24011.754  | < 2e-16 ***
BODYMASSINDEX | 0.56950 | 0.09925 | 5.738  | 9.58e-09 ***
PEDIGREEFUNC | 0.30632          | 0.09948 | 3.079  | 0.00208 **

En este modelo todas las variables son estadisticamente relevantes.

Ahora vamos a comparar todos los modelos de regresión logistica que hemos visto para ver su eficacia.

TEST | accuracy | kappa | sensitivity | specificity
|---|---|---|---|---|
Todas las variables | 0.7949 | 0.5556  | 0.7692 | 0.8077
PREGNANT + GLUCOSE + SKINTHICKNESS + BODYMASSINDEX + PEDIGREEFUNC | 0.8019 | 0.5616 | 0.7429 | 0.8310
PREGNANT + GLUCOSA + BODYMASSINDEX + PEDIGREEFUNC | 0.7517 | 0.4511 | 0.6346 | 0.8144


Teoricamente los modelos de regresión siempre mejoran al añadir más variables aunque sea solo marginalmente. Sin embargo, en este caso eliminar variables nos ha permitido acceder a una mayor cantidad de datos y de esta forma mejorar el rendimiento, como se ve en el segundo modelo. Sin embargo tener acceso a más datos no parece haber sido suficiente para el tercer modelo que ha perdido accuracy, y sobre todo, sensitivity.


### Modelo Knn

El algoritmo knn consiste en utilizar la información de los k vecinos más próximos para clasificar ejemplos no etiquetados.

Aplicamos el modelo KNN con una serie de valores k en el rango 3:115, el código nos mostrará el accuracy según el k y eligirá el mejor valor.

Vamos a probar con diferentes grupos de variables según el análisis exploratorio.

Estos son los grupos de variables que hemos utilizado junto con sus resultados:

TEST | accuracy | kappa | sensitivity | specificity
|---|---|---|---|---|
INSULINA+GLUCOSA+EDAD+PRESION SANGUINEA | 0.7179 | 0.3774 | 0.6154 | 0.7692
INSULINA+GLUCOSA+EDAD | 0.7564 | 0.4956 | 0.8077 | 0.7308
INSULINA+GLUCOSA+EDAD+PEDIGREE | 0.7436 | 0.4444 | 0.6923 | 0.7692
INSULINA+GLUCOSA | 0.7051 | 0.3784 | 0.6923 | 0.7115
GLUCOSA+EDAD+PEDIGREE | 0.7692 | 0.4706 | 0.6154 | 0.8462

Tras esta primera aproximación, decidimos quedarnos con las variables INSULINA+GLUCOSA+EDAD por tener los mejores valores de accuracy, kappa, sensitivity y especificity.

Aquí mostramos la accuracy en estas variables sobre los diferentes valores de k.

<center>
  <figure>
    <img src="k_39_knn.png" alt=".." title="Fig 2" height="230"/>
    <figcaption>Fig 3. Accuracy según valores de k</figcaption>
  </figure>
</center>

Finalmente, seleccionamos como la mejor opción un modelo de knn utilizando las variables Insulina, Glucosa y Edad con k=39, lo que nos proporciona una accuracy aproximada en test del 0.75.

### Árbol de decisión

Un árbol de decisión consiste en dividir por decisiones lógicas un conjunto etiquetado, para luego aplicar esas decisiones a un conjunto no etiquetado.

Para empezar, vamos a efectuar un primer árbol de decisión utilizando todas la variables y el parametro de complejidad en el rango de 0 a 0.1 en pasos de 0.0005.

En este caso, selecciona un parámetro de complejidad óptimo 0.0005 y nos muestra la siguiente gráfica de influencia por variable en el resultado.

<center>
  <figure>
    <img src="DependenciaVarsArbol.png" alt=".." title="Fig 2" height="220"/>
    <figcaption>Fig 4. Influencia de cada variable para cp=0.0005</figcaption>
  </figure>
</center>

Tras ver esta gráfica, decidimos repetir el proceso con el grupo de variables Glucose, Insulin, Age, y BodyMassIndex, ya que obtenemos los mismos resultados.

TEST | accuracy | kappa | sensitivity | specificity
|---|---|---|---|---|
Todas las variables | 0.7436 | 0.4444  | 0.6923 | 0.7692
INSULINA+GLUCOSA+EDAD+BODYMASSINDEX | 0.7436 | 0.4444 | 0.6923 | 0.7692

Este es el árbol de decisión resultante, con cp = 0.0005

<center>
  <figure>
    <img src="arbolFinal.png" alt=".." title="Fig 2" height="370"/>
    <figcaption>Fig 5. Arbol final para cp=0.0005</figcaption>
  </figure>
</center>

### Support Vector Machines

Las Support Vector Machines (SVMs) son algoritmos de clasificación que funcionan dividiendo el espacio de observaciones usando un hiperplano. Este hiperplano puede usar varias formulas. Nosotros hemos usado lineal y radial.

#### SVM lineal

Como hemos venido haciendo hasta ahora comenzaremos con el modelo más elemental, usar todas las variables explicativas. Para buscar el hiperparametro hemos realizado dos barridos, uno amplio, probando en (0.01, 0.1, 1, 10, 100, 1000) para descubrir el orden del coste, y otros más fino alrededor del valor que ha salido en el primer barrido, al final hemos obtenido C = 0.03

<center>
  <figure>
    <img src="Cost_vs_accuracy_svml1.png" height="200">  <img src="Cost_vs_accuracy_svml2.png" height="200">
    <figcaption>Fig 6. Barrido amplio (en escala logaritmica) y barrido fino</figcaption>
  </figure>
</center>

Ajustado el modelo "simple" queda la cuestión de buscar las variables correctas. Al contrario que otros el modelo SVM no da ninguna pista sobre que variables usar asi que para no buscar sin rumbo hemos decidido probar con GLUCOSE, INSULIN y AGE ya que son las variables que han resultado más relevantes en otros modelos y con PREGNANT, GLUCOSE, BODYMASSINDEX, PEDIGREEFUNC que son las variables que el unico modelo lineal aparte de este, la regresión logistica, consideraba relevantes.

Estos han sido los resultados. Tener en cuenta que para todos los SVMs el valor optimo para los hipervalores puede cambiar dependiendo de la seed que hayas usado para separar el conjunto de training o para entrenar el modelo, el cambio no suele ser muy grande asi que estos sirven como valores orientativos.

Test | Coste | Accuracy | Sensibility | Specificity
| --- | --- | --- | --- | --- 
| Todas las variables | 0.03 | 0.7692 | 0.7308 | 0.7885
| GLUCOSE, INSULIN y AGE | 5.7 | 0.7179 | 0.6923 | 0.7308
| PREGNANT, GLUCOSE, BODYMASSINDEX y PEDIGREEFUNC | 0.9 | 0.7821 | 0.8432 | 0.7500
Sin lugar a dudas el mejor modelo parece se el que usa PREGNANT, GLUCOSE, BODYMASSINDEX y PEDIGREEFUNC.

#### SVM radial

Como antes vamos a empezar haciendo el modelo basico con todas las variables antes de empezar a elegir variables. En este caso es más complicado ajustar los hiperparametros por que hay dos, el coste y la sigma. Sin embargo el metodo para optimizar los hiperparametros va a ser el mismo, comenzar con un barrido amplio y pasar a otros más finos.

<center>
  <figure>
    <img src="Cost_vs_accuracy_svmr1.png" height ="270" width="320">
    <figcaption>Fig 7. Barrido amplio. La línea morada alcanza 100% de accuracy, lo que probablemente signifique sobreentrenamiento.</figcaption>
  </figure>
</center>

Este modelo ha requerido más esfuerzo para encontrar sus hiperparametros ya que ha presentado gran tendencia ha sobreentrenarse, sobre todo para valores de Sigma mayores a 0.1.

Ahora pasemos a elegir variables, otra vez para no andar sin rumbo vamos a probar GLUCOSE, INSULIN y AGE.  Comparemos los resultados:

Lineal | Coste | Accuracy | Sensibility | Specificity
| --- | --- | --- | --- | --- 
| Todas las variables | 0.03 | 0.7692 | 0.7308 | 0.7885
| GLUCOSE, INSULIN y AGE | 5.7 | 0.7179 | 0.6923 | 0.7308
| PREGNANT, GLUCOSE, BODYMASSINDEX y PEDIGREEFUNC | 0.9 | 0.7821 | 0.8432 | 0.7500

Radial | Coste | Sigma | Accuracy | Sensibility | Specificity
| --- | --- | --- | --- | --- | ---
Todas las variables | 7 | 0.0005 | 0.7564 | 0.6923 | 0.7885
GLUCOSE, INSULIN y AGE | 100 | 0.011 | 0.8077 | 0.8462 | 0.7885
El modelo que ha obtenido mejores resultados es el SVM radial usando GLUCOSE, INSULIN y AGE aunque solo supera por un estrecho margen al SVM lineal con PREGNANT, GLUCOSE, BODYMASSINDEX y PEDIGREEFUNC.

### Redes neuronales

Las redes neuronales son un modelo de clasificación que funcionan procesando variables en pequeñas unidades independientes interconectadas.

En este caso, vamos a intentar optimizar nuestro modelo con una única capa intermedia con n neuronas y luego seleccionar las variables necesarias.

En primer lugar, ejecutamos diferentes modelos con todas las variables, el número de neuronas en un rango de 5 a 25 en pasos de 5. Como medida de prevención de sobreentrenamiento vamos a utilizar weight decay, con el rango de valores 0, 0.0001,0.001,0.01,0.1,1.

Este el resultado de accuracy dependiendo del número de neuronas y weight decay:

<center>
  <figure>
    <img src="neuronas_vs_weight_decay.png" alt=".." title="Fig 2" height = "270" width = "360"/>
    <figcaption>Fig 8. Accuracy dependiendo del n_neuronas y el weight decay</figcaption>
  </figure>
</center>

Al final nos decantamos por 25 neuronas con un weight decay de 0.1, ya que valores inferiores daban como resultado modelos sobreentrenados.

Una vez que conocemos la configuración de nuestro modelo, procedeemos a ejecutarlo con todas las variables y analizar su sensibilidad. Así podremos retirar aquellas variables poco importantes.

<center>
  <figure>
    <img src="Sensibilidad.png" alt=".." title="Fig 2" height="350"/>
    <figcaption>Fig 9. Sensibilidad por variable</figcaption>
  </figure>
</center>
Finalmente seleccionamos las variables AGE, GLUCOSE, INSULIN y PREGNANT.

Y ejecutamos el modelo para esas variables. Dado que nos da muy buenos valores medios y un histograma de probabilidad favorable, nos quedamos con este modelo.

TEST | accuracy | kappa | sensitivity | specificity
|---|---|---|---|---|
Todas las variables | 0.7436 | 0.4545  | 0.7308 | 0.7500 
AGE + GLUCOSE + INSULIN + PREGNANT | 0.8077 | 0.5946  | 0.8462 | 0.7885

<center>
  <figure>
    <img src="histogramaRedes.png" alt=".." title="Fig 2" height = "300"/>
    <figcaption>Fig 10. Conteo de probabilidad de clase</figcaption>
</figure>
</center>

## Comparación de modelos

Para esta comparación hemos seguido el siguiente procedimiento. Hemos creado un archivo de R donde se entrenaban y evaluaban los mejores modelos de cada tipo, fijando una semilla para los números aleatorios al principio del documento. Despues hemos ejecutado el documento entero varias veces cambiando la seed en cada iteración y apuntando los resultados.

SEED | MODELO | Training Accuracy | Training Sensitivity | Test Accuracy | Test Sensitivity
 --- | --- | --- | --- | --- | --- |
 1 | KNN | 0.7919 | 0.8365 | 0.7692 | 0.8462 |
 1 | Árbol | 0.8589 | 0.8750 | 0.7949 | 0.802587 |
 1 | Red Neuronal | 0.8445 | 0.8462 | 0.8077 | 0.8077 |
 1 | Regresión Logística | 0.7847 | 0.7692 | 0.7692 | 0.7692 |
 1 | SVN | 0.7943 | 0.8173 | 0.7821 | 0.8462 |
 10 | KNN | 0.7919 |    0.8221   | 0.7179 | 0.8846|
 10 | Árbol |0.9163|0.9231| 0.7436 | 0.7308 |
 10 | Red Neuronal | 0.8469  | 0.8269 | 0.8205| 0.8462  |
 10 | Regresión Logística | 0.7823 | 0.7596   | 0.7692| 0.8846  |
 10 | SVN | 0.7919 | 0.7885 | 0.8205   |  0.9615 |
 100 | KNN |  0.799   | 0.8125    | 0.7179 | 0.6538  |
 100 | Árbol | 0.8684 | 0.9423 | 0.7436| 0.8462 |
 100 | Red Neuronal | 0.866| 0.8558 | 0.7308 | 0.5385 |
 100 | Regresión Logística | 0.8134| 0.8077|  0.7051  |  0.5769  |
 100 | SVN | 0.8206 | 0.7885  | 0.6538   | 0.4231 |
 1000 | KNN | 0.7775 | 0.8125| 0.7949 | 0.7692 |
 1000 | Árbol | 0.8349 | 0.9231 |  0.7564  | 0.7692   |
 1000 | Red Neuronal | 0.8445  |  0.8365  | 0.8462 | 0.7692  |
 1000 | Regresión Logística | 0.7727| 0.7596 | 0.7821  |  0.7692 |
 1000 | SVN |  0.8014| 0.8077  |0.8205 | 0.7692 |
 10000 | KNN | 0.7847 | 0.8183  | 0.7564|0.8077 |
 10000 | Árbol |  0.878|  0.8942| 0.7308 |0.6923 |
 10000 | Red Neuronal | 0.8517 | 0.8365| 0.7949  |0.7308|
 10000 | Regresión Logística |  0.799  | 0.7981|0.7564  | 0.6923  |
 10000 | SVN |0.811|  0.7981 |  0.7436 |0.7308   |

 SEED | MODELO | Training Accuracy | Training Sensitivity | Test Accuracy | Test Sensitivity
 --- | --- | --- | --- | --- | --- |
 Media | KNN | 0.789 | 0.8365 | 0.7513 | 0.7923 |
 Media | Árbol | 0.8713 | 0.9115 | 0.7539 | 0.7682 |
 Media | Red Neuronal | 0.8507 | 0.8404 | 0.8000 | 0.7385 |
 Media | Regresión Logística | 0.7904 | 0.8077 | 0.7564 | 0.7384 |
 Media | SVN | 0.8038 | 0.8000 | 0.7641 | 0.7462 |
 
 En general, en training los arboles y las redes neuronales han sido los mejores modelos con diferencia, siendo los arboles algo mejores en accuracy y, sobre todo, bastante más sensibles. Sin embargo, los arboles han empeorado mucho a la hora de los test mientras que las redes neuronales han sido mucho más consistentes, eso sí, a pesar de la buena accuracy de las redes neuronales, tienden a ser poco sensibles, así que si necesitamos sensibilidad tal vez deberíamos usar un modelo de KNN. 