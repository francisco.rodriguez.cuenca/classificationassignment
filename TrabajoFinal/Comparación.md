# Tabla de comparación de algoritmos en función de la seed

SEED | MODELO | Training Accuracy | Training Sensitivity | Test Accuracy | Test Sensitivity
 --- | --- | --- | --- | --- | --- |
 1 | KNN | 0.7919 | 0.8365 | 0.7692 | 0.8462 |
 1 | Árbol | 0.8589 | 0.8750 | 0.7949 | 0.802587 |
 1 | Red Neuronal | 0.8445 | 0.8462 | 0.8077 | 0.8077 |
 1 | Regresión Logística | 0.7847 | 0.7692 | 0.7692 | 0.7692 |
 1 | SVN | 0.7943 | 0.8173 | 0.7821 | 0.8462 |
 10 | KNN | 0.7919 |    0.8221   | 0.7179 | 0.8846|
 10 | Árbol |0.9163|0.9231| 0.7436 | 0.7308 |
 10 | Red Neuronal | 0.8469  | 0.8269 | 0.8205| 0.8462  |
 10 | Regresión Logística | 0.7823 | 0.7596   | 0.7692| 0.8846  |
 10 | SVN | 0.7919 | 0.7885 | 0.8205   |  0.9615 |
 100 | KNN |  0.799   | 0.8125    | 0.7179 | 0.6538  |
 100 | Árbol | 0.8684 | 0.9423 | 0.7436| 0.8462 |
 100 | Red Neuronal | 0.866| 0.8558 | 0.7308 | 0.5385 |
 100 | Regresión Logística | 0.8134| 0.8077|  0.7051  |  0.5769  |
 100 | SVN | 0.8206 | 0.7885  | 0.6538   | 0.4231 |
 1000 | KNN | 0.7775 | 0.8125| 0.7949 | 0.7692 |
 1000 | Árbol | 0.8349 | 0.9231 |  0.7564  | 0.7692   |
 1000 | Red Neuronal | 0.8445  |  0.8365  | 0.8462 | 0.7692  |
 1000 | Regresión Logística | 0.7727| 0.7596 | 0.7821  |  0.7692 |
 1000 | SVN |  0.8014| 0.8077  |0.8205 | 0.7692 |
 10000 | KNN | 0.7847 | 0.8077  | 0.7564|0.8077 |
 10000 | Árbol |  0.878|  0.8942| 0.7308 |0.6923 |
 10000 | Red Neuronal | 0.8517 | 0.8365| 0.7949  |0.7308|
 10000 | Regresión Logística |  0.799  | 0.7981|0.7564  | 0.6923  |
 10000 | SVN |0.811|  0.7981 |  0.7436 |0.7308   |